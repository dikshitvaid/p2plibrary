/**
 * 
 */
package com.xebia.p2p.service;

import com.xebia.p2p.model.Inventory;
import com.xebia.p2p.model.LoanableItem;

/**
 * @author dikshitv-l
 *
 */
public interface LoanService<LI extends LoanableItem<LI, I>, I extends Inventory<LI, I>> {

	public LI request(String item);

	public LI surrender(LI item);
}
