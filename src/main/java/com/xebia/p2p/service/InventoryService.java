/**
 * 
 */
package com.xebia.p2p.service;

import java.util.List;

import com.xebia.p2p.model.Inventory;
import com.xebia.p2p.model.LoanableItem;

/**
 * @author dikshitvpublicl
 *
 */
public interface InventoryService<LI extends LoanableItem<LI,I>, I extends Inventory<LI, I>> {

	public List<I> inventories();

	public I inventory(Long ownerId);

	public I createInventory(I inventory);

	public I updateInventory(I inventory);

	public I removeInventory(Long inventoryId);

	public I addItem(Long inventoryId, LI item);

	public I updateItem(Long inventoryId, Long itemId, LI item);

	public LI searchItem(Long inventoryId, String item);

	public I removeItem(Long bookShelfId, Long titleId);

}
