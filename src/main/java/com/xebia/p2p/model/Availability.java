/**
 * 
 */
package com.xebia.p2p.model;

import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.xebia.p2p.library.model.entity.Bookshelf;
import com.xebia.p2p.library.model.entity.LibraryUser;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dikshitv-l
 *
 */
@NoArgsConstructor
@Embeddable
@Getter
@Setter
public class Availability<LI extends LoanableItem<LI, I>, I extends Inventory<LI, I>> {

	@Column
	@Basic
	private Integer quantity;
	@Column
	@Basic
	private Integer daysToLoan;

	@Column
	@Enumerated(value = EnumType.STRING)
	private Status status = Status.AVAILABLE;

	@Column(name = "loan_start_date")
	@Type(type = "org.hibernate.type.LocalDateTimeType")
	private LocalDateTime loanStartDate;

	@Column(name = "loan_end_date")
	@Type(type = "org.hibernate.type.LocalDateTimeType")
	private LocalDateTime loanEndDate;

	@ManyToOne(targetEntity = LibraryUser.class, fetch = FetchType.EAGER, cascade = { CascadeType.MERGE,
			CascadeType.PERSIST })
	@JoinColumn(name = "lender_id", referencedColumnName = "id")
	@JsonDeserialize(as = LibraryUser.class)
	// @JsonIgnore
	private Lender<LI, I> owner;

	@ManyToOne(targetEntity = LibraryUser.class, fetch = FetchType.EAGER, cascade = { CascadeType.MERGE,
			CascadeType.PERSIST })
	@JoinColumn(name = "borrower_id", referencedColumnName = "id")
	@JsonDeserialize(as = LibraryUser.class)
	// @JsonIgnore
	private Borrower<LI, I> borrower;

	@ManyToOne(targetEntity = Bookshelf.class, fetch = FetchType.EAGER, cascade = { CascadeType.MERGE,
			CascadeType.PERSIST })
	@JoinColumn(name = "inventory_id", referencedColumnName = "id")
	@JsonDeserialize(as = Bookshelf.class)
	// @JsonIgnore
	private Inventory<LI, I> inventory;

	public static enum Status {
		AVAILABLE, LOANED, OVER_DUE;
	}
}
