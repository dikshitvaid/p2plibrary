/**
 * 
 */
package com.xebia.p2p.model;

import java.util.Map;

/**
 * @author dikshitv-l
 *
 */
public interface Inventory<LI extends LoanableItem<LI, I>, I extends Inventory<LI, I>> {

	public Lender<LI, I> owner();

	public Map<Long, LI> items();

	public Map<Long, LI> updateAvailability(Availability<LI, I> availability);

	public Status status();

	static enum Status {
		ACTIVE, INACTIVE;
	}

	boolean isActive();

	void addItem(LI item);
}
