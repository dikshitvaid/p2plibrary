/**
 * 
 */
package com.xebia.p2p.model;

/**
 * @author dikshitv-l
 *
 */
public interface LoanableItem<LI extends LoanableItem<LI, I>, I extends Inventory<LI, I>> {

	public String title();

	public Availability<LI, I> availability();
}
