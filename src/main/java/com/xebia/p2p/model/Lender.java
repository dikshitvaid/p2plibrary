/**
 * 
 */
package com.xebia.p2p.model;

import java.util.List;

/**
 * @author dikshitv-l
 *
 */
public interface Lender<LI extends LoanableItem<LI, I>, I extends Inventory<LI, I>> {

	public I inventory();

	public boolean canLend(String item);

	public LI lend(String borrower, String item);

	public List<LI> lentItems();
}
