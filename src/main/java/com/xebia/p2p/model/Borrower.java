/**
 * 
 */
package com.xebia.p2p.model;

import java.util.List;

/**
 * @author dikshitv-l
 *
 */
public interface Borrower<LI extends LoanableItem<LI, I>, I extends Inventory<LI, I>> {

	public boolean canBorrow();

	public LI borrow(String item);

	public List<LI> borrowedItems();
}
