package com.xebia.p2p;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.xebia.p2p"})
public class P2PLibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(P2PLibraryApplication.class, args);
	}

}

