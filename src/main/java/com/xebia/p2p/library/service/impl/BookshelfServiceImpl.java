/**
 * 
 */
package com.xebia.p2p.library.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xebia.p2p.library.model.entity.Book;
import com.xebia.p2p.library.model.entity.Bookshelf;
import com.xebia.p2p.library.repository.BookRepository;
import com.xebia.p2p.library.repository.BookshelfRepository;
import com.xebia.p2p.library.service.BookshelfService;
import com.xebia.p2p.model.Availability.Status;

/**
 * @author dikshitv-l
 *
 */
@Service
public class BookshelfServiceImpl implements BookshelfService {

	@Autowired
	private BookshelfRepository bookshelfRepo;

	@Autowired
	private BookRepository bookRepo;

	@Override
	public List<Bookshelf> inventories() {
		final List<Bookshelf> bookshelfs = bookshelfRepo.findAll();
		if (null == bookshelfs || bookshelfs.isEmpty()) {
			throw new EntityNotFoundException("No bookshelfs found.");
		}
		return bookshelfs;
	}

	@Override
	public Bookshelf inventory(Long bookshelfId) {
		Optional<Bookshelf> bookshelfOpt = bookshelfRepo.findById(bookshelfId);
		return extract(bookshelfOpt);
	}

	private Bookshelf extract(Optional<Bookshelf> bookshelfOpt) {
		if (bookshelfOpt.isPresent()) {
			return bookshelfOpt.get();
		} else {
			throw new EntityNotFoundException("Bookshelf not found");
		}
	}

	@Override
	public Bookshelf createInventory(Bookshelf inventory) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bookshelf updateInventory(Bookshelf inventory) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bookshelf removeInventory(Long inventoryId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bookshelf addItem(Long bookShelfId, Book book) {
		final Bookshelf bookshelf = findBookshelf(bookShelfId);
		prepareBook(book, bookshelf);
		final Book bookPersisted = bookRepo.save(book);
		bookshelf.addItem(bookPersisted);
		return bookshelfRepo.save(bookshelf);
	}

	@Override
	public Bookshelf updateItem(Long bookshelfId, Long bookId, Book book) {
		final Bookshelf bookshelf = findBookshelf(bookshelfId);
		book.setId(bookId);
		bookshelf.addItem(book);
		return bookshelfRepo.save(bookshelf);
	}

	@Override
	public Book searchItem(Long bookshelfId, String book) {
		final Bookshelf bookshelf = findBookshelf(bookshelfId);
		return bookshelf.findTitle(book);
	}

	@Override
	public Bookshelf removeItem(Long bookShelfId, Long titleId) {
		final Bookshelf bookshelf = findBookshelf(bookShelfId);
		bookshelf.items().remove(titleId);
		return bookshelfRepo.save(bookshelf);
	}

	private Bookshelf findBookshelf(Long bookShelfId) {
		final Optional<Bookshelf> bookshelfOpt = bookshelfRepo.findById(bookShelfId);
		final Bookshelf bookshelf = extract(bookshelfOpt);
		if (!bookshelf.isActive()) {
			throw new EntityNotFoundException("Bookshelf inactive.");
		}
		return bookshelf;
	}

	private void prepareBook(Book book, Bookshelf bookshelf) {
		book.getAvailability().setOwner(bookshelf.getOwner());
		book.getAvailability().setInventory(bookshelf);
		;
	}

	@Override
	public List<Bookshelf> searchItem(Book book) {
		List<Bookshelf> bookshelves = inventories();
		List<Bookshelf> foundBookshelves = new ArrayList<>();

		for (Bookshelf bookshelf : bookshelves) {
			Bookshelf foundBookshelf = new Bookshelf();
			Predicate<Map.Entry<Long, Book>> predicate = new Predicate<Map.Entry<Long, Book>>() {

				@Override
				public boolean test(Entry<Long, Book> m) {
					return m.getValue().availability().getStatus() == Status.AVAILABLE
							&& m.getValue().getTitle().toLowerCase().equals(book.getTitle().toLowerCase());
				}
			};
			Map<Long, Book> filteredMap = bookshelf.items().entrySet().stream().filter(predicate).collect(Collectors
					.toMap(e -> ((Map.Entry<Long, Book>) e).getKey(), e -> ((Map.Entry<Long, Book>) e).getValue()));
			if (filteredMap.isEmpty()) {
				throw new EntityNotFoundException("Book " + book.getTitle() + " not found.");
			}
			foundBookshelf.setId(bookshelf.getId());
			foundBookshelf.setOwner(bookshelf.getOwner());
			foundBookshelf.setStatus(bookshelf.getStatus());
			foundBookshelf.setItems(filteredMap);
			foundBookshelves.add(foundBookshelf);
		}
		// TODO: update found book status to LOANED
		return foundBookshelves;
	}

}
