/**
 * 
 */
package com.xebia.p2p.library.service;

import java.util.List;

import com.xebia.p2p.library.model.entity.Book;
import com.xebia.p2p.library.model.entity.Bookshelf;
import com.xebia.p2p.service.InventoryService;

/**
 * @author dikshitv-l
 *
 */
public interface BookshelfService extends InventoryService<Book, Bookshelf> {

	List<Bookshelf> searchItem(Book book);

}
