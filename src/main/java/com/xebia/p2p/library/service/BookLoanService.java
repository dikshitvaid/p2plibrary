/**
 * 
 */
package com.xebia.p2p.library.service;

import com.xebia.p2p.library.model.entity.Book;
import com.xebia.p2p.library.model.entity.Bookshelf;
import com.xebia.p2p.service.LoanService;

/**
 * @author dikshitv-l
 *
 */
public interface BookLoanService extends LoanService<Book, Bookshelf> {

}
