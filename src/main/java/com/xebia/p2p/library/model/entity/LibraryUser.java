/**
 * 
 */
package com.xebia.p2p.library.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.xebia.p2p.library.model.BookBorrower;
import com.xebia.p2p.library.model.BookLender;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dikshitv-l
 *
 */
@NoArgsConstructor
@Entity
@Getter
@Setter
@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
public class LibraryUser extends AbstractEntity implements BookLender, BookBorrower {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@OneToOne(targetEntity = Bookshelf.class, fetch = FetchType.LAZY, mappedBy = "owner")
	private Bookshelf bookshelf;

	@OneToMany(targetEntity = Book.class, fetch = FetchType.LAZY, mappedBy = "availability.owner")
	private List<Book> lentItems = new ArrayList<>();

	@OneToMany(targetEntity = Book.class, fetch = FetchType.LAZY, mappedBy = "availability.borrower")
	private List<Book> borrowedItems = new ArrayList<>();

	@Override
	public Bookshelf inventory() {
		return this.bookshelf;
	}

	@Override
	public boolean canLend(String item) {
		return true;
	}

	@Override
	public Book lend(String borrower, String item) {
		return null;
	}

	@Override
	public List<Book> lentItems() {
		return this.lentItems;
	}

	@Override
	public boolean canBorrow() {
		return false;
	}

	@Override
	public Book borrow(String item) {
		return null;
	}

	@Override
	public List<Book> borrowedItems() {
		return this.borrowedItems;
	}
}
