/**
 * 
 */
package com.xebia.p2p.library.model.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.xebia.p2p.model.Availability;
import com.xebia.p2p.model.Inventory;
import com.xebia.p2p.model.Lender;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dikshitv-l
 *
 */
@NoArgsConstructor
@Entity
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@id")
public class Bookshelf extends AbstractEntity implements Inventory<Book, Bookshelf> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3480164805002038953L;

	@OneToOne(targetEntity = LibraryUser.class, cascade = { CascadeType.MERGE,
			CascadeType.PERSIST }, fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id")
	private Lender<Book, Bookshelf> owner;

	@OneToMany(targetEntity = Book.class, fetch = FetchType.EAGER, cascade = { CascadeType.MERGE,
			CascadeType.PERSIST }, mappedBy = "availability.inventory")
	// @JsonManagedReference("inventory_books")
	private Map<Long, Book> items = new HashMap<>();

	@Basic
	@Enumerated(EnumType.STRING)
	@Column
	private Status status = Status.ACTIVE;

	@Override
	public Lender<Book, Bookshelf> owner() {
		return this.owner;
	}

	@Override
	public Map<Long, Book> items() {
		return this.getItems();
	}

	@Override
	public Map<Long, Book> updateAvailability(Availability<Book, Bookshelf> availability) {
		return this.items;
	}

	@Override
	public Status status() {
		return this.status;
	}

	@Override
	@JsonIgnore
	public boolean isActive() {
		return this.status() == Status.ACTIVE ? true : false;
	}

	@Override
	public void addItem(Book book) {
		final Map<Long, Book> books = this.getItems();
		books.put(book.getId(), book);
	}

	public Book findTitle(String title) {
		List<Book> books = this.items().entrySet().stream()
				.filter(m -> m.getValue().getTitle().toLowerCase().equals(title.toLowerCase())).map(e -> e.getValue())
				.collect(Collectors.toList());
		if (books.isEmpty()) {
			throw new EntityNotFoundException("Requested book " + title + " not found.");
		}
		return books.get(0);
	}
}
