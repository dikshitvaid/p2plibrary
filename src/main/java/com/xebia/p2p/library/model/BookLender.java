/**
 * 
 */
package com.xebia.p2p.library.model;

import com.xebia.p2p.library.model.entity.Book;
import com.xebia.p2p.library.model.entity.Bookshelf;
import com.xebia.p2p.model.Lender;

/**
 * @author dikshitv-l
 *
 */
public interface BookLender extends Lender<Book,Bookshelf> {
	

}
