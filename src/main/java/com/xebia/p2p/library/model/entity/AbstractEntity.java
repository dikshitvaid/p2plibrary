/**
 * 
 */
package com.xebia.p2p.library.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * @author dikshitv-l
 *
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AbstractEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5111437227963684329L;

	@CreatedBy
	@JsonIgnore
	@Column(updatable = false)
	private Integer createdBy;

	@CreatedDate
	@JsonIgnore
	@Type(type = "org.hibernate.type.LocalDateTimeType")
	@Column(updatable = false)
	private LocalDateTime createdAt;

	@LastModifiedBy
	@JsonIgnore
	private Integer updatedBy;

	@LastModifiedDate
	@Type(type = "org.hibernate.type.LocalDateTimeType")
	@JsonIgnore
	private LocalDateTime updatedAt;

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
}
