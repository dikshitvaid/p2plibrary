/**
 * 
 */
package com.xebia.p2p.library.model.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.xebia.p2p.model.Availability;
import com.xebia.p2p.model.LoanableItem;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dikshitv-l
 *
 */
@NoArgsConstructor
@Entity
@Getter
@Setter
@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
public class Book extends AbstractEntity implements LoanableItem<Book, Bookshelf> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6301477976289847104L;

	@Column
	@Basic
	private String title;

	@Embedded
	private Availability<Book, Bookshelf> availability;

	@Override
	public String title() {
		return this.title;
	}

	@Override
	public Availability<Book, Bookshelf> availability() {
		return this.availability;
	}
}
