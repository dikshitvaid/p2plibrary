/**
 * 
 */
package com.xebia.p2p.library.repository;

import com.xebia.p2p.library.model.entity.Book;
import com.xebia.p2p.library.model.entity.Bookshelf;
import com.xebia.p2p.repository.LonableItemRepository;

/**
 * @author dikshitv-l
 *
 */
public interface BookRepository extends LonableItemRepository<Book, Bookshelf> {

}
