/**
 * 
 */
package com.xebia.p2p.library.repository;

import org.springframework.stereotype.Repository;

import com.xebia.p2p.library.model.entity.Book;
import com.xebia.p2p.library.model.entity.Bookshelf;
import com.xebia.p2p.repository.PeerRepository;

/**
 * @author dikshitv-l
 *
 */
@Repository
public interface LibraryUserRepository extends PeerRepository<Book, Bookshelf>{

}
