/**
 * 
 */
package com.xebia.p2p.library.repository;

import org.springframework.stereotype.Repository;

import com.xebia.p2p.library.model.entity.Book;
import com.xebia.p2p.library.model.entity.Bookshelf;
import com.xebia.p2p.model.Lender;
import com.xebia.p2p.repository.InventoryRepository;

/**
 * @author dikshitv-l
 *
 */
@Repository
public interface BookshelfRepository extends InventoryRepository<Book, Bookshelf> {

	Bookshelf findByOwner(Lender<Book, Bookshelf> owner);

}
