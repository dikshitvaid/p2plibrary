/**
 * 
 */
package com.xebia.p2p.library.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xebia.p2p.library.model.entity.Book;
import com.xebia.p2p.library.model.entity.Bookshelf;
import com.xebia.p2p.library.service.BookshelfService;

/**
 * @author dikshitv-l
 *
 */
@RestController
@RequestMapping("/bookshelfs")
public class BookshelfController {

	@Autowired
	private BookshelfService bookShelfService;

	@GetMapping
	public List<Bookshelf> getBookshleves() {
		return bookShelfService.inventories();
	}

	@PostMapping
	@RequestMapping("/{bookShelfId}/titles/")
	public Bookshelf addTitle(@PathVariable("bookShelfId") Long bookShelfId, @Valid @RequestBody Book book) {
		return bookShelfService.addItem(bookShelfId, book);
	}

	@PostMapping
	public List<Bookshelf> findBook(@Valid @RequestBody Book book) {
		List<Bookshelf> foundBookshelves = bookShelfService.searchItem(book);
		// TODO: - return books instead
		return foundBookshelves;
	}

	@PutMapping
	@RequestMapping("/{bookShelfId}/titles/{titleId}")
	public Bookshelf removeTitle(@PathVariable("bookShelfId") Long bookShelfId,
			@PathVariable("bookShelfId") Long titleId, @Valid @RequestBody(required = false) Book book) {
		if (null != book) {
			return bookShelfService.updateItem(bookShelfId, titleId, book);
		} else {
			return bookShelfService.removeItem(bookShelfId, titleId);
		}
	}

	@GetMapping
	@RequestMapping("/{bookShelfId}/{title}")
	public Book getBookFromBookshelf(@PathVariable("bookShelfId") Long bookShelfId,
			@PathVariable("title") String title) {
		return bookShelfService.searchItem(bookShelfId, title);
	}

	@GetMapping
	@RequestMapping("/{bookShelfId}")
	public Bookshelf getBookshlelf(@PathVariable("bookShelfId") Long bookShelfId) {
		return bookShelfService.inventory(bookShelfId);
	}

}
