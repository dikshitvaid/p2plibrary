/**
 * 
 */
package com.xebia.p2p.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.xebia.p2p.model.Inventory;
import com.xebia.p2p.model.LoanableItem;

/**
 * @author dikshitv-l
 *
 */
@NoRepositoryBean
public interface PeerRepository<LI extends LoanableItem<LI, I>, I extends Inventory<LI, I>>
		extends JpaRepository<LI, Long> {

}
