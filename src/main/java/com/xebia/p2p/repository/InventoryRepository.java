/**
 * 
 */
package com.xebia.p2p.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.xebia.p2p.model.Inventory;
import com.xebia.p2p.model.LoanableItem;

/**
 * @author dikshitv-l
 *
 */
@NoRepositoryBean
public interface InventoryRepository<LI extends LoanableItem<LI, I>, I extends Inventory<LI, I>>
		extends JpaRepository<I, Long> {

}
